lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .settings(
    Common.settings,
    name := """bike-sharing-kata""",
    libraryDependencies += guice
  )
  .aggregate(
    `registration-infrastructure`,
    `registration-application`,
    `registration-domain-services`,
    `registration-domain-model`,
    `rental-infrastructure`,
    `rental-application`,
    `rental-domain`,
    `accounting-infrastructure`,
    `accounting-application`,
    `accounting-domain`
  )
  .dependsOn(`registration-infrastructure`, `rental-infrastructure`, `accounting-infrastructure`)

lazy val `registration-infrastructure` = (project in file("./modules/registration/infrastructure"))
  .enablePlugins(PlayScala)
  .settings(
    Common.settings,
    name := """bike-sharing-registration-infrastructure"""
  )
  .dependsOn(`registration-application`, `registration-domain-services`, `registration-domain-model`)

lazy val `registration-application` = (project in file("./modules/registration/application"))
  .settings(
    Common.settings,
    name := """bike-sharing-registration-application"""
  )
  .dependsOn(`registration-domain-services`, `registration-domain-model`)

lazy val `registration-domain-services` = (project in file("./modules/registration/domain/services"))
  .settings(
    Common.settings,
    name := """bike-sharing-registration-domain-services"""
  )
  .dependsOn(`registration-domain-model`)

lazy val `registration-domain-model` = (project in file("./modules/registration/domain/model"))
  .settings(
    Common.settings,
    name := """bike-sharing-registration-domain-model"""
  )

lazy val `rental-infrastructure` = (project in file("./modules/rental/infrastructure"))
  .enablePlugins(PlayScala)
  .settings(
    Common.settings,
    name := """bike-sharing-rental-infrastructure"""
  )
  .dependsOn(`rental-application`)

lazy val `rental-application` = (project in file("./modules/rental/application"))
  .settings(
    Common.settings,
    name := """bike-sharing-rental-application"""
  )
  .dependsOn(`rental-domain`)

lazy val `rental-domain` = (project in file("./modules/rental/domain"))
  .settings(
    Common.settings,
    name := """bike-sharing-rental-domain"""
  )

lazy val `accounting-infrastructure` = (project in file("./modules/accounting/infrastructure"))
  .enablePlugins(PlayScala)
  .settings(
    Common.settings,
    name := """bike-sharing-accounting-infrastructure"""
  )
  .dependsOn(`accounting-application`)

lazy val `accounting-application` = (project in file("./modules/accounting/application"))
  .settings(
    Common.settings,
    name := """bike-sharing-accounting-application"""
  )
  .dependsOn(`accounting-domain`)

lazy val `accounting-domain` = (project in file("./modules/accounting/domain"))
  .settings(
    Common.settings,
    name := """bike-sharing-accounting-domain"""
  )