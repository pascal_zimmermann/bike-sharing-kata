package controllers.registration

import ch.zpascal.bikesharing.registration.domain.service.InMemoryUserRegistrationRepository

object InMemoryRepositories {

  val UserRegistrationRepository = new InMemoryUserRegistrationRepository

}
