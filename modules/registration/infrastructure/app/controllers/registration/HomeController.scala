package controllers.registration

import ch.zpascal.bikesharing.registration.application._
import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, UserId, VerificationCode}
import ch.zpascal.bikesharing.registration.domain.service.{UserRegistrationRepository, VerificationCodeSender}
import javax.inject.Inject
import models.registration.UserRegistrationData
import play.api.data.Form
import play.api.data.Forms.{email, mapping}
import play.api.i18n.I18nSupport
import play.api.mvc.{Action, AnyContent, BaseController, ControllerComponents}

class HomeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController with I18nSupport {
  implicit private val inMemoryUserRegistrationRepository: UserRegistrationRepository = InMemoryRepositories.UserRegistrationRepository
  implicit private val systemOutVerificationCodeSender: VerificationCodeSender = new SystemOutVerificationCodeSender

  def index: Action[AnyContent] = Action { implicit request =>
    Ok(views.html.registration.index())
  }

  def login: Action[AnyContent] = Action { implicit request =>
    Ok("called registration login")
  }

  def register: Action[AnyContent] = Action { implicit request =>
    Ok(views.html.registration.register(userRegistrationForm))
  }

  def startRegistrationProcess: Action[AnyContent] = Action { implicit request =>
    userRegistrationForm.bindFromRequest.fold(
      withErrors => {
        BadRequest("Invalid data")
      },
      data => {
        val userRegistrationService = new RegistrationService
        userRegistrationService.startUserRegistration(RegistrationRequest(EMailAddress(data.eMailAddress)))
        Redirect(routes.HomeController.pendingRegistration())
      })
  }

  def pendingRegistration: Action[AnyContent] = Action { implicit request =>
    Ok("Registration almost complete. Just click the verification link you received.")
  }

  def verifyRegistration(userId: String, verificationCode: String): Action[AnyContent] = Action { implicit request =>
    val verificationService = new VerificationService
    verificationService.verifyRegistration(VerificationRequest(UserId(userId), VerificationCode(verificationCode))) match {
      case Verified => Ok("Registration completed")
      case UnknownUser => BadRequest(s"User $userId is unknown")
      case InvalidVerificationCode => BadRequest(s"Verification code $verificationCode is incorrect")
    }
  }

  val userRegistrationForm = Form(
    mapping(
      "eMailAddress" -> email
    )(UserRegistrationData.apply)(UserRegistrationData.unapply)
  )

}
