package controllers.registration

import ch.zpascal.bikesharing.registration.domain.model.UserRegistration
import ch.zpascal.bikesharing.registration.domain.service.VerificationCodeSender
import constants.registration.Constants

class SystemOutVerificationCodeSender extends VerificationCodeSender {
  override def send(userRegistration: UserRegistration): Unit = {
    val host = "localhost:9000/"
    val arguments = userRegistration.userId.value + "/" + userRegistration.verificationCode.value
    val verificationLink = host + Constants.Root + "/verifyRegistration/" + arguments
    println(s"Here is your verification link: $verificationLink")
  }
}
