package ch.zpascal.bikesharing.registration.domain.model

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class EMailAddressTest extends AnyFlatSpec with Matchers {
  "EMailAddress" must "have valid format" in {
    assertThrows[IllegalArgumentException](EMailAddress("test"))
    assertThrows[IllegalArgumentException](EMailAddress(""))
    EMailAddress("test@example.com").value shouldBe "test@example.com"
  }
}
