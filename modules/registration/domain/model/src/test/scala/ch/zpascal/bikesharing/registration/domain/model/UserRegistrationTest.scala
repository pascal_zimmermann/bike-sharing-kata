package ch.zpascal.bikesharing.registration.domain.model

import org.scalatest.OptionValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class UserRegistrationTest extends AnyFlatSpec with Matchers with OptionValues {

  "verify" should "return a registered user if the verification was successful" in {
    val maybeRegisteredUser = new UserRegistration(UserId("u1"), EMailAddress("test@gmail.com"), VerificationCode("1234")).verify(VerificationCode("1234"))
    maybeRegisteredUser.value shouldBe a[RegisteredUser]
    maybeRegisteredUser.value should have(userId(UserId("u1")))
  }
  it should "return nothing if the verification code does not match" in {
    new UserRegistration(UserId("u1"), EMailAddress("test@gmail.com"), VerificationCode("1234")).verify(VerificationCode("123")) shouldBe None
  }

  val userId = Symbol("userId")
}
