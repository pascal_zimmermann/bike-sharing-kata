package ch.zpascal.bikesharing.registration.domain.model

case class VerificationCode(value: String)
