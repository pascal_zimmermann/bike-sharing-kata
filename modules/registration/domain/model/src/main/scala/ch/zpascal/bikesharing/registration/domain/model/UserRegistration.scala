package ch.zpascal.bikesharing.registration.domain.model

class UserRegistration(val userId: UserId, val eMailAddress: EMailAddress, val verificationCode: VerificationCode) {
  def verify(verificationCode: VerificationCode): Option[RegisteredUser] =
    if (verificationCode == this.verificationCode) Some(new RegisteredUser(userId))
    else None
}
