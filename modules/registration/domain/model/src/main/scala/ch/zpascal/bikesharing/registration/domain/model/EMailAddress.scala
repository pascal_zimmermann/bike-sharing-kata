package ch.zpascal.bikesharing.registration.domain.model

case class EMailAddress(value: String) {
  require(value contains "@")
}
