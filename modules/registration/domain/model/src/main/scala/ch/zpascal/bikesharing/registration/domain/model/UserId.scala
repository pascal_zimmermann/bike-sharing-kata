package ch.zpascal.bikesharing.registration.domain.model

case class UserId(value: String)
