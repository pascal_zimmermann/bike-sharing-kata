package ch.zpascal.bikesharing.registration.domain.service

import java.util.UUID

import ch.zpascal.bikesharing.registration.domain.model.UserId

trait UserIdGenerator {
  def generate(): UserId
}

class UuidUserIdGenerator extends UserIdGenerator {
  override def generate(): UserId = UserId(UUID.randomUUID().toString)
}