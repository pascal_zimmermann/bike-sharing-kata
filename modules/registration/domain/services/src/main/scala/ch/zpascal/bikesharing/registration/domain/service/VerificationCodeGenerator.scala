package ch.zpascal.bikesharing.registration.domain.service

import java.util.UUID

import ch.zpascal.bikesharing.registration.domain.model.VerificationCode

trait VerificationCodeGenerator {
  def generateRandom(): VerificationCode
}

class UuidVerificationCodeGenerator extends VerificationCodeGenerator {
  override def generateRandom(): VerificationCode = VerificationCode(UUID.randomUUID().toString)
}
