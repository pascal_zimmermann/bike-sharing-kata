package ch.zpascal.bikesharing.registration.domain.service

import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, RegisteredUser, UserId, UserRegistration}

class InMemoryUserRegistrationRepository extends UserRegistrationRepository {
  private var userRegistrationStorage: Map[UserId, UserRegistration] = Map.empty
  private var registeredUserStore: Map[UserId, RegisteredUser] = Map.empty

  override def findByEMail(eMailAddress: EMailAddress): Option[UserRegistration] =
    userRegistrationStorage.values.find(_.eMailAddress == eMailAddress)

  override def save(userRegistration: UserRegistration): Unit = userRegistrationStorage =
    userRegistrationStorage + (userRegistration.userId -> userRegistration)

  override def load(userId: UserId): Option[UserRegistration] =
    userRegistrationStorage.get(userId)

  override def save(registeredUser: RegisteredUser): Unit =
    registeredUserStore = registeredUserStore + (registeredUser.userId -> registeredUser)

  override def loadRegistered(userId: UserId): Option[RegisteredUser] =
    registeredUserStore.get(userId)
}
