package ch.zpascal.bikesharing.registration.domain.service

import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, UserRegistration}

class UserRegistrationService(implicit userRegistrationRepository: UserRegistrationRepository,
                              verificationCodeSender: VerificationCodeSender,
                              userIdGenerator: UserIdGenerator = new UuidUserIdGenerator,
                              verificationCodeGenerator: VerificationCodeGenerator = new UuidVerificationCodeGenerator) {
  def createRegistration(eMailAddress: EMailAddress): UserRegistration =
    userRegistrationRepository.findByEMail(eMailAddress) match {
      case Some(_) => throw new UserAlreadyRegistered
      case None =>
        val userRegistration = new UserRegistration(userIdGenerator.generate(), eMailAddress, verificationCodeGenerator.generateRandom())
        verificationCodeSender.send(userRegistration)
        userRegistration
    }

}
