package ch.zpascal.bikesharing.registration.domain.service

import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, RegisteredUser, UserId, UserRegistration}

trait UserRegistrationRepository {

  def findByEMail(eMailAddress: EMailAddress): Option[UserRegistration]

  def save(userRegistration: UserRegistration): Unit

  def load(userId: UserId): Option[UserRegistration]

  def save(registeredUser: RegisteredUser): Unit

  def loadRegistered(userId: UserId): Option[RegisteredUser]

}
