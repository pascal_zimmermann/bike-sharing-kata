package ch.zpascal.bikesharing.registration.domain.service

import ch.zpascal.bikesharing.registration.domain.model.UserRegistration

trait VerificationCodeSender {

  def send(userRegistration: UserRegistration): Unit

}
