package ch.zpascal.bikesharing.registration.domain.service

import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, UserId, UserRegistration, VerificationCode}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class UserRegistrationServiceTest extends AnyFlatSpec with Matchers {

  var sentVerifications: List[EMailAddress] = Nil
  implicit val userRegistrationRepository: UserRegistrationRepository = new InMemoryUserRegistrationRepository
  implicit val userIdGenerator: UserIdGenerator = () => UserId("User42")
  implicit val verificationCodeGenerator: VerificationCodeGenerator = () => VerificationCode("123456")
  implicit val verificationCodeSender: VerificationCodeSender =
    (userRegistration: UserRegistration) => sentVerifications = userRegistration.eMailAddress :: sentVerifications
  userRegistrationRepository.save(new UserRegistration(UserId("u1"), EMailAddress("alreadyregisterd@gmail.com"), VerificationCode("xxx")))
  val userRegistrationService = new UserRegistrationService

  "createRegistration" should "create a UserRegistration" in {
    userRegistrationService.createRegistration(EMailAddress("testuser@gmail.com")) shouldBe a[UserRegistration]
  }
  it should "create a random verification code" in {
    userRegistrationService.createRegistration(EMailAddress("testuser@gmail.com")) should have(verificationCode(VerificationCode("123456")))
  }
  it should "create a random user id" in {
    userRegistrationService.createRegistration(EMailAddress("testuser@gmail.com")) should have(userId(UserId("User42")))
  }
  it should "send the verification code" in {
    sentVerifications = Nil
    userRegistrationService.createRegistration(EMailAddress("testuser@gmail.com"))
    sentVerifications shouldBe List(EMailAddress("testuser@gmail.com"))
  }
  it should "throw an exception when the email address is already registred" in {
    an[UserAlreadyRegistered] shouldBe thrownBy(userRegistrationService.createRegistration(EMailAddress("alreadyregisterd@gmail.com")))
  }

  val verificationCode = Symbol("verificationCode")
  val userId = Symbol("userId")
}
