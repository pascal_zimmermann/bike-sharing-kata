package ch.zpascal.bikesharing.registration.application

import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, UserId, UserRegistration, VerificationCode}
import ch.zpascal.bikesharing.registration.domain.service.{InMemoryUserRegistrationRepository, UserRegistrationRepository}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class VerificationServiceTest extends AnyFlatSpec with Matchers {

  implicit val userRegistrationRepository: UserRegistrationRepository = new InMemoryUserRegistrationRepository
  val verificationService = new VerificationService
  userRegistrationRepository.save(new UserRegistration(UserId("user5"), EMailAddress("test@gmail.com"), VerificationCode("1234")))
  userRegistrationRepository.save(new UserRegistration(UserId("user3"), EMailAddress("test2@gmail.com"), VerificationCode("123")))

  "verifyUserRegistration" should "store a registered user" in {
    verificationService.verifyRegistration(VerificationRequest(UserId("user5"), VerificationCode("1234"))) shouldBe Verified
    userRegistrationRepository.loadRegistered(UserId("user5")) shouldBe defined
  }
  it should "reject verification if the verification code does not match" in {
    verificationService.verifyRegistration(VerificationRequest(UserId("user3"), VerificationCode("1234"))) shouldBe InvalidVerificationCode
  }
  it should "reject verification if the user is not known" in {
    verificationService.verifyRegistration(VerificationRequest(UserId("user4"), VerificationCode("1234"))) shouldBe UnknownUser
  }

}
