package ch.zpascal.bikesharing.registration.application

import ch.zpascal.bikesharing.registration.domain.model.{EMailAddress, UserRegistration}
import ch.zpascal.bikesharing.registration.domain.service.{InMemoryUserRegistrationRepository, UserRegistrationRepository, VerificationCodeSender}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class RegistrationServiceTest extends AnyFlatSpec with Matchers {

  implicit val userRegistrationRepository: UserRegistrationRepository = new InMemoryUserRegistrationRepository
  implicit val verificationCodeSender: VerificationCodeSender = (_: UserRegistration) => ()

  val registrationService = new RegistrationService

  "registerUser" should "create a new UserRegistration" in {
    val eMailAddress = EMailAddress("test@gmail.com")
    registrationService.startUserRegistration(RegistrationRequest(eMailAddress))
    userRegistrationRepository.findByEMail(eMailAddress) shouldBe a[Some[_]]
  }

}
