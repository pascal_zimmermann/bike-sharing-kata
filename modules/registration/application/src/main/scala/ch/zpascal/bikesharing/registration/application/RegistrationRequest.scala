package ch.zpascal.bikesharing.registration.application

import ch.zpascal.bikesharing.registration.domain.model.EMailAddress

case class RegistrationRequest(eMailAddress: EMailAddress)
