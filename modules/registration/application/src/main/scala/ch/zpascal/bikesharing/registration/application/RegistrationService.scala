package ch.zpascal.bikesharing.registration.application

import ch.zpascal.bikesharing.registration.domain.service.{UserRegistrationRepository, UserRegistrationService, VerificationCodeSender}

class RegistrationService(implicit userRegistrationRepository: UserRegistrationRepository,
                          verificationCodeSender: VerificationCodeSender) {
  private val userRegistrationService = new UserRegistrationService

  def startUserRegistration(request: RegistrationRequest): Unit =
    userRegistrationRepository.save(userRegistrationService.createRegistration(request.eMailAddress))

}