package ch.zpascal.bikesharing.registration.application

import ch.zpascal.bikesharing.registration.domain.model.{UserId, VerificationCode}

case class VerificationRequest(userId: UserId, verificationCode: VerificationCode)