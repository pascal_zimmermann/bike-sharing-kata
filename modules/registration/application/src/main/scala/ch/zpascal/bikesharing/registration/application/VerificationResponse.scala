package ch.zpascal.bikesharing.registration.application

sealed trait VerificationResponse

case object InvalidVerificationCode extends VerificationResponse

case object Verified extends VerificationResponse

case object UnknownUser extends VerificationResponse
