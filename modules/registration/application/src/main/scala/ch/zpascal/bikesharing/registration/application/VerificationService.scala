package ch.zpascal.bikesharing.registration.application

import ch.zpascal.bikesharing.registration.domain.service.UserRegistrationRepository

class VerificationService(implicit userRegistrationRepository: UserRegistrationRepository) {
  def verifyRegistration(request: VerificationRequest): VerificationResponse =
    userRegistrationRepository.load(request.userId).map(_.verify(request.verificationCode)) match {
      case None => UnknownUser
      case Some(None) => InvalidVerificationCode
      case Some(Some(registeredUser)) =>
        userRegistrationRepository.save(registeredUser)
        Verified
    }

}
