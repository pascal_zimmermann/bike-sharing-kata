import sbt.Def.Setting
import sbt.Keys._

object Common {
  val settings: Seq[Setting[_]] = Seq(
    organization := "ch.zpascal",
    version := "1.0.0-RC",
    scalaVersion := "2.13.1",
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )
  )
}